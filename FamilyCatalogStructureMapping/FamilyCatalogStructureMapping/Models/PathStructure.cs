﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FamilyCatalogStructureMapping.Models
{
    class PathStructure
    {
        public string FamilyName { get; set; }
        public string ParameterTemplate { get; set; }
        public string CategoryName { get; set; }
        public string DisciplineName { get; set; }
        public string Prefix { get; set; }
        public string TgaAdditionalInfo { get; set; }
        public string EltAdditionalInfo { get; set; }
        public string NewName { get; set; }
        public string RevitCategory { get; set; }

        public PathStructure(string familyName,
                             string parameterTemplate,
                             string categoryName,
                             string disciplineName,
                             string prefix,
                             string tgaAddInfo,
                             string eltAddInfo,
                             string newName,
                             string revitCategory)
        {
            FamilyName = familyName;
            ParameterTemplate = parameterTemplate;
            CategoryName = categoryName;
            DisciplineName = disciplineName;
            Prefix = prefix;
            TgaAdditionalInfo = tgaAddInfo;
            EltAdditionalInfo = eltAddInfo;
            NewName = newName;
            RevitCategory = revitCategory;
        }
    }
}
