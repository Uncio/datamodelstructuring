﻿using FamilyCatalogStructureMapping.Properties;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FamilyCatalogStructureMapping.Models.Helpers
{
    class XlsFileService
    {
        public List<Models.PathStructure> OpenFile(string fileName,
                                                   string columnName,
                                                   string prefixColumnName,
                                                   string addInfoColumn,
                                                   ref string addInfoColumnName,
                                                   string newNameColumn,
                                                   bool isElt = false)
        {
            // Family catalog
            var csvFileName = fileName.Remove(fileName.LastIndexOf("\\")) + "\\tempCSV1_.csv";

            CsvHelper.ConvertExcelToCsv(fileName, csvFileName, Resources.ExcelSheetName);

            var result = new List<Models.PathStructure>();
            bool start = false;
            using (var reader = new StreamReader(csvFileName, Encoding.GetEncoding("iso-8859-1")))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');

                    if (values[8].Equals("Familienname PDBS") && !start)
                    {
                        start = true;
                        if (!String.IsNullOrEmpty(addInfoColumn) && Enum.IsDefined(typeof(ColumnConverter), addInfoColumn.ToUpper()))
                        {
                            addInfoColumnName = values[(int)Enum.Parse(typeof(ColumnConverter), addInfoColumn.ToUpper())];
                        } 
                        continue;
                    }
                    if (!start)
                        continue;

                    var prefix = String.Empty;
                    var addInfo = String.Empty;
                    var newName = String.Empty;
                    if (!String.IsNullOrEmpty(newNameColumn) && Enum.IsDefined(typeof(ColumnConverter), newNameColumn.ToUpper()))
                    {
                        newName = values[(int)Enum.Parse(typeof(ColumnConverter), newNameColumn.ToUpper())];
                    }

                    if (!String.IsNullOrEmpty(columnName))
                    {
                        var isApplicable = false;
                        if (Enum.IsDefined(typeof(ColumnConverter), columnName.ToUpper()))
                            isApplicable = !String.IsNullOrEmpty(values[(int)Enum.Parse(typeof(ColumnConverter), columnName.ToUpper())]);
                        if (isApplicable)
                        {
                            if (!String.IsNullOrEmpty(prefixColumnName) && Enum.IsDefined(typeof(ColumnConverter), prefixColumnName.ToUpper()))
                            {
                                prefix = values[(int)Enum.Parse(typeof(ColumnConverter), prefixColumnName.ToUpper())];
                            }
                            if (!String.IsNullOrEmpty(newNameColumn) && Enum.IsDefined(typeof(ColumnConverter), newNameColumn.ToUpper()))
                            {
                                newName = values[(int)Enum.Parse(typeof(ColumnConverter), newNameColumn.ToUpper())];
                            }
                            var famName = values[8];
                            var parameterTemplate = String.Empty;
                            if (!isElt)
                            {
                                parameterTemplate = values[10];
                                if (parameterTemplate.Contains("/"))
                                    parameterTemplate = parameterTemplate.Replace("/", "_");
                            }                         
                            var categoryName = values[3];
                            if (categoryName.Contains("/"))
                                categoryName = categoryName.Replace("/", "_");
                            var disciplineName = values[2];
                            if (disciplineName.Contains("/"))
                                disciplineName = disciplineName.Replace("/", "_");
                            var revitCategory = String.Empty;
                            if (isElt)
                                revitCategory = values[10];
                            else
                                revitCategory = values[9];
                            if (revitCategory.Contains("/"))
                                revitCategory = revitCategory.Replace("/", "_");
                            if (isElt)
                                result.Add(
                                    new PathStructure(famName,
                                                      parameterTemplate,
                                                      categoryName,
                                                      disciplineName,
                                                      prefix,
                                                      String.Empty,
                                                      addInfo,
                                                      newName,
                                                      revitCategory));
                            else
                                result.Add(
                                    new PathStructure(famName,
                                                      parameterTemplate,
                                                      categoryName,
                                                      disciplineName,
                                                      prefix,
                                                      addInfo,
                                                      String.Empty,
                                                      newName,
                                                      revitCategory));
                        }
                    }
                    else
                    {
                        var famName = values[8];
                        var parameterTemplate = String.Empty;
                        if (!isElt)
                        {
                            parameterTemplate = values[10];
                            if (parameterTemplate.Contains("/"))
                                parameterTemplate = parameterTemplate.Replace("/", "_");
                        }
                        var categoryName = values[3];
                        if (categoryName.Contains("/"))
                            categoryName = categoryName.Replace("/", "_");
                        var disciplineName = values[2];
                        if (disciplineName.Contains("/"))
                            disciplineName = disciplineName.Replace("/", "_");
                        var revitCategory = String.Empty;
                        if (isElt)
                            revitCategory = values[10];
                        else
                            revitCategory = values[9];
                        if (isElt)
                            result.Add(
                                new PathStructure(famName,
                                                  parameterTemplate,
                                                  categoryName,
                                                  disciplineName,
                                                  prefix,
                                                  String.Empty,
                                                  addInfo,
                                                  newName,
                                                  revitCategory));
                        else
                            result.Add(
                                new PathStructure(famName,
                                                  parameterTemplate,
                                                  categoryName,
                                                  disciplineName,
                                                  prefix,
                                                  addInfo,
                                                  String.Empty,
                                                  newName,
                                                  revitCategory));
                    }
                }
            }

            File.Delete(csvFileName);

            return result;
        }

        public void SaveFile(string fileName, object content)
        {
        }
    }
}
