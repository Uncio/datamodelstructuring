﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace FamilyCatalogStructureMapping.Models.Helpers
{
    class CsvHelper
    {
        public static void ConvertExcelToCsv(string fileName, string csvFileName, string wsName)
        {
            if (File.Exists(csvFileName)) File.Delete(csvFileName);
            Excel.Application excelApp = new Excel.Application();
            excelApp.Visible = false;

            try
            {
                Excel.Workbook workbook = excelApp.Workbooks.Open(fileName);
                Excel.Worksheet dataSheet = (Excel.Worksheet)excelApp.Workbooks[1].Worksheets[wsName];
                dataSheet.Cells.Replace(",", "_");
                dataSheet.SaveAs(csvFileName, Excel.XlFileFormat.xlCSV);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(workbook);
            }
            finally
            {
                excelApp.DisplayAlerts = false;
                excelApp.Quit();
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(excelApp);
            }
        }
    }
}
