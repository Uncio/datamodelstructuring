﻿using System;
using System.Windows.Input;

namespace EU.Plandata.LegendenTool.ViewModels
{
    class ParameterCommand : ICommand
    {
        readonly Action<object> _execute;
        readonly Func<bool> _canExecute;

        public ParameterCommand(Action<object> execute, Func<bool> canExecute = null)
        {
            this._execute = execute;

            if (canExecute != null)
                this._canExecute = canExecute;
        }

        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, new EventArgs());
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            if (_canExecute != null)
                return _canExecute();
            return true;
        }

        public void Execute(object parameter = null)
        {
            this._execute(parameter);
        }
    }
}
