﻿using EU.Plandata.Common.WPF.ViewModels;
using FamilyCatalogStructureMapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FamilyCatalogStructureMapping.ViewModels
{
    class PathStructureVM: NotifyPropertyChangedBaseVM
    {
        private string familyName;
        public string FamilyName { get => familyName; set => Set(ref familyName, value); }
        private string parameterTemplate;
        public string ParameterTemplate { get => parameterTemplate; set => Set(ref parameterTemplate, value); }
        private string categoryName;
        public string CategoryName { get => categoryName; set => Set(ref categoryName, value); }
        private string disciplineName;
        public string DisciplineName { get => disciplineName; set => Set(ref disciplineName, value); }
        private string revitCategory;
        public string RevitCategory { get => revitCategory; set => Set(ref revitCategory, value); }
        private bool isSelected;
        public bool IsSelected { get => isSelected; set => Set(ref isSelected, value); }
        private string prefix;
        public string Prefix { get => prefix; set => Set(ref prefix, value); }
        private string tgaAdditionalInfo;
        public string TgaAdditionalInfo { get => tgaAdditionalInfo; set => Set(ref tgaAdditionalInfo, value); }
        private string eltAdditionalInfo;
        public string EltAdditionalInfo { get => eltAdditionalInfo; set => Set(ref eltAdditionalInfo, value); }
        private string familyNewName;
        public string FamilyNewName { get => familyNewName; set => Set(ref familyNewName, value); }

        public PathStructureVM(string familyName,
                               string parameterTemplate,
                               string categoryName,
                               string disciplineName,
                               string prefix,
                               string TgaAddInfo,
                               string EltAddInfo,
                               string NewName,
                               string revitCategory)
        {
            FamilyName = familyName;
            ParameterTemplate = parameterTemplate;
            CategoryName = categoryName;
            DisciplineName = disciplineName;
            IsSelected = true;
            Prefix = prefix;
            TgaAdditionalInfo = TgaAddInfo;
            EltAdditionalInfo = EltAddInfo;
            FamilyNewName = NewName;
            RevitCategory = revitCategory;
        }


        public static List<PathStructureVM> ParseFromModel(List<PathStructure> files)
        {
            var result = new List<PathStructureVM>();
            files.ForEach(x => result.Add(
                new PathStructureVM(x.FamilyName,
                                    x.ParameterTemplate,
                                    x.CategoryName,
                                    x.DisciplineName,
                                    x.Prefix,
                                    x.TgaAdditionalInfo,
                                    x.EltAdditionalInfo,
                                    x.NewName,
                                    x.RevitCategory)));
            return result;
        }

        public static List<PathStructure> ParseToModel(List<PathStructureVM> files)
        {
            var result = new List<PathStructure>();
            files.ForEach(x => result.Add(new PathStructure(x.FamilyName,
                                                            x.ParameterTemplate,
                                                            x.CategoryName,
                                                            x.DisciplineName,
                                                            x.Prefix,
                                                            x.TgaAdditionalInfo,
                                                            x.EltAdditionalInfo,
                                                            x.FamilyNewName,
                                                            x.RevitCategory)));
            return result;
        }
    }
}
