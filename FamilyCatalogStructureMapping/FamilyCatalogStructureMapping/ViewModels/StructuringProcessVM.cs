﻿using EU.Plandata.Common.WPF.ViewModels;
using EU.Plandata.LegendenTool.ViewModels;
using FamilyCatalogStructureMapping.Models.Helpers;
using FamilyCatalogStructureMapping.Properties;
using FamilyCatalogStructureMapping.Views;
using Microsoft.Win32;
using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace FamilyCatalogStructureMapping.ViewModels
{
    class StructuringProcessVM: NotifyPropertyChangedBaseVM
    {
        private List<string> familyFiles = new List<string>();
        private string log = String.Empty;
        public bool isNotProcess = true;
        public bool IsNotProcess { get => isNotProcess; set => Set(ref isNotProcess, value); }
        public List<PathStructureVM> Files { get; set; } = new List<PathStructureVM>();
        public ICommand OpenTGAFamilyCatalogCommand { get; set; }
        public ICommand OpenELTFamilyCatalogCommand { get; set; }
        public ICommand SelectRootFolderCommand { get; set; }
        public ICommand ApplyCommand { get; set; }
        public ICommand SelectFamiliesFolderCommand { get; set; }

        public ObservableCollection<PathStructureVM> FamiliesCollection { get; set; } = new ObservableCollection<PathStructureVM>();
        private bool tgaSelected = false;
        public bool TgaSelected { get => tgaSelected; set => Set(ref tgaSelected, value); }
        private bool eltSelected = false;
        public bool EltSelected { get => eltSelected; set => Set(ref eltSelected, value); }

        private string tgaPath = Properties.Settings.Default.TgaPath;
        public string TgaPath { get => tgaPath; set => Set(ref tgaPath, value); }
        private string eltPath = Properties.Settings.Default.EltPath;
        public string EltPath { get => eltPath; set => Set(ref eltPath, value); }
        private string rootFolder = Properties.Settings.Default.RootFolder;
        public string RootFolder 
        {
            get { return rootFolder; }
            set
            {
                Set(ref rootFolder, value);
                CheckPaths();
            }
        }
        private string tgaCompanyColumn;
        public string TgaCompanyColumn { get => tgaCompanyColumn; set => Set(ref tgaCompanyColumn, value); }
        private string eltCompanyColumn;
        public string EltCompanyColumn { get => eltCompanyColumn; set => Set(ref eltCompanyColumn, value); }
        private string tgaPrefixColumn;
        public string TgaPrefixColumn { get => tgaPrefixColumn; set => Set(ref tgaPrefixColumn, value); }
        private string eltPrefixColumn;
        public string EltPrefixColumn { get => eltPrefixColumn; set => Set(ref eltPrefixColumn, value); }
        private string tgaAdditionalInfoColumn;
        public string TgaAdditionalInfoColumn { get => tgaAdditionalInfoColumn; set => Set(ref tgaAdditionalInfoColumn, value); }
        private string eltAdditionalInfoColumn;
        public string EltAdditionalInfoColumn { get => eltAdditionalInfoColumn; set => Set(ref eltAdditionalInfoColumn, value); }
        private string tgaAdditionalColumnName = Resources.Column_AdditionalInfromation;
        public string TgaAdditionalColumnName { get => tgaAdditionalColumnName; set => Set(ref tgaAdditionalColumnName, value); }
        private string eltAdditionalColumnName = Resources.Column_AdditionalInfromation;
        public string EltAdditionalColumnName { get => eltAdditionalColumnName; set => Set(ref eltAdditionalColumnName, value); }
        private string tgaNewNameColumn;
        public string TgaNewNameColumn { get => tgaNewNameColumn; set => Set(ref tgaNewNameColumn, value); }
        private string eltNewNameColumn;
        public string EltNewNameColumn { get => eltNewNameColumn; set => Set(ref eltNewNameColumn, value); }

        private string familiesFolder = Properties.Settings.Default.FamiliesFolder;
        public string FamiliesFolder
        {
            get { return familiesFolder; }
            set
            {
                Set(ref familiesFolder, value);
                CheckPaths();
            }
        }

        private bool arePathsSetted;
        public bool ArePathsSetted { get => arePathsSetted; set => Set(ref arePathsSetted, value); }
        private bool isBackEnabled = true;
        public bool IsBackEnabled { get => isBackEnabled; set => Set(ref isBackEnabled, value); }



        public StructuringProcessVM()
        {
            OpenTGAFamilyCatalogCommand = new ParameterCommand(OpenTGAFamilyCatalog);
            OpenELTFamilyCatalogCommand = new ParameterCommand(OpenELTFamilyCatalog);
            SelectRootFolderCommand = new ParameterCommand(SelectRootFolder);
            ApplyCommand = new ParameterCommand(Apply);
            SelectFamiliesFolderCommand = new ParameterCommand(SelectFamiliesFolder);

            CheckPaths();
        }

        private void OpenTGAFamilyCatalog(object parameters)
        {
            var openDialog = new OpenFileDialog();
            if (!String.IsNullOrEmpty(Properties.Settings.Default.TgaPath))
            {
                var initialPath = Properties.Settings.Default.TgaPath.Remove(Properties.Settings.Default.TgaPath.LastIndexOf("\\"));
                openDialog.InitialDirectory = initialPath;
            }
            openDialog.Title = "Selecting TGA Family Catalog...";
            openDialog.Filter = "xlsx files (*.xlsx)|*.xlsx|All files (*.*)|*.*";            
            if (openDialog.ShowDialog() == true)
            {
                TgaPath = openDialog.FileName;
                Properties.Settings.Default.TgaPath = TgaPath;
                Properties.Settings.Default.Save();
            }
        }
        private void OpenELTFamilyCatalog(object parameters)
        {
            var openDialog = new OpenFileDialog();
            if (!String.IsNullOrEmpty(Properties.Settings.Default.EltPath))
            {
                var initialPath = Properties.Settings.Default.EltPath.Remove(Properties.Settings.Default.EltPath.LastIndexOf("\\"));
                openDialog.InitialDirectory = initialPath;
            }
            openDialog.Title = "Selecting ELT Family Catalog...";
            openDialog.Filter = "xlsx files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            if (openDialog.ShowDialog() == true)
            {
                EltPath = openDialog.FileName;
                Properties.Settings.Default.EltPath = EltPath;
                Properties.Settings.Default.Save();
            }
        }
        private void SelectRootFolder(object parameters)
        {
            var openDialog = new CommonOpenFileDialog();
            var initialPath = Properties.Settings.Default.RootFolder;
            openDialog.InitialDirectory = initialPath;
            openDialog.IsFolderPicker = true;
            openDialog.Title = Resources.DialogTitle_MainFolder;
            if (openDialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                RootFolder = openDialog.FileName;
                Properties.Settings.Default.RootFolder = RootFolder;
                Properties.Settings.Default.Save();
            }
        }
        private void SelectFamiliesFolder(object parameters)
        {
            var openDialog = new CommonOpenFileDialog();
            var initialPath = Properties.Settings.Default.FamiliesFolder;
            openDialog.InitialDirectory = initialPath;
            openDialog.IsFolderPicker = true;
            openDialog.Title = Resources.DialogTitle_TargetFolder;
            if (openDialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                FamiliesFolder = openDialog.FileName;
                Properties.Settings.Default.FamiliesFolder = FamiliesFolder;
                Properties.Settings.Default.Save();
            }
        }
        public void ReadExcelData()
        {
            FamiliesCollection.Clear();
            var xlsService = new XlsFileService();
            if (TgaSelected)
            {
                var tgaAddInfo = String.Empty;
                var collection = xlsService.OpenFile(TgaPath,
                                                     TgaCompanyColumn,
                                                     TgaPrefixColumn,
                                                     TgaAdditionalInfoColumn,
                                                     ref tgaAddInfo,
                                                     TgaNewNameColumn);
                TgaAdditionalColumnName = tgaAddInfo;
                PathStructureVM.ParseFromModel(collection).ForEach(x => FamiliesCollection.Add(x));
            }
            if (EltSelected)
            {
                var eltAddInfo = String.Empty;
                var collection = xlsService.OpenFile(EltPath,
                                                     EltCompanyColumn,
                                                     EltPrefixColumn,
                                                     EltAdditionalInfoColumn,
                                                     ref eltAddInfo,
                                                     EltNewNameColumn,
                                                     true);
                EltAdditionalColumnName = eltAddInfo;
                PathStructureVM.ParseFromModel(collection).ForEach(x => FamiliesCollection.Add(x));
            }
        }
        public void CollectFamilyFiles()
        {
            familyFiles.Clear();
            if (Directory.Exists(FamiliesFolder))
                familyFiles.AddRange(Directory.GetFiles(FamiliesFolder, "*.rfa", SearchOption.AllDirectories));
        }

        private void Apply(object parameters)
        {
            IsNotProcess = false;
            if (ArePathsSetted &&
                (TgaSelected && !String.IsNullOrEmpty(TgaPath) && File.Exists(TgaPath)) ||
                (EltSelected && !String.IsNullOrEmpty(EltPath) && File.Exists(EltPath)))
            {
                var myThread = new Thread(DivideAsync);
                myThread.Start();
            }
            else
                MainWindow.Current.NotificationService.Warning(Resources.FoldersErrorMessage);
        }

        private async void DivideAsync()
        {
            IsBackEnabled = false;
            string[] tempFamilyFiles = new string[familyFiles.Count()];
            familyFiles.CopyTo(tempFamilyFiles);
            PathStructureVM[] tempFamCollection = new PathStructureVM[FamiliesCollection.Count()];
            FamiliesCollection.CopyTo(tempFamCollection,0);
            IsBackEnabled = true;
            foreach (var file in tempFamilyFiles)
            {
                var famName = file.Remove(0, file.LastIndexOf("\\") + 1);
                famName = famName.Remove(famName.LastIndexOf(".rfa"));

                var famData = tempFamCollection.FirstOrDefault(x => x.FamilyName.Equals(famName));
                if (famData != null && famData.IsSelected)
                {
                    var disciplineFolder = Path.Combine(RootFolder, famData.DisciplineName);
                    if (!Directory.Exists(disciplineFolder))
                        Directory.CreateDirectory(disciplineFolder);
                    var categoryFolder = Path.Combine(disciplineFolder, famData.CategoryName);
                    if (!Directory.Exists(categoryFolder))
                        Directory.CreateDirectory(categoryFolder);
                    var revitCatName = Path.Combine(categoryFolder, famData.RevitCategory);
                    if (!Directory.Exists(revitCatName))
                        Directory.CreateDirectory(revitCatName);
                    var templateFolder = Path.Combine(revitCatName, famData.ParameterTemplate);
                    if (!Directory.Exists(templateFolder))
                        Directory.CreateDirectory(templateFolder);
                    var newFileName = String.Empty;
                    if (!String.IsNullOrEmpty(famData.FamilyNewName))
                        famName = famData.FamilyNewName;
                    if (!String.IsNullOrEmpty(famData.Prefix))
                        newFileName = Path.Combine(templateFolder, famData.Prefix + "_" + famName + ".rfa");
                    else
                        newFileName = Path.Combine(templateFolder, famName + ".rfa");

                    if (!File.Exists(newFileName))
                        File.Copy(file, newFileName);
                    else
                        log += famName + Resources.LogMessage2 + Environment.NewLine;
                }
                else
                    log += famName + Resources.LogMessage1 + Environment.NewLine;
            }
            IsNotProcess = true;

            if (!String.IsNullOrEmpty(log))
                File.WriteAllText(Path.Combine(Properties.Settings.Default.RootFolder, "log.txt"), log);
        }

        private void CheckPaths()
        {
            ArePathsSetted = false;
            if (!String.IsNullOrEmpty(RootFolder) && !String.IsNullOrEmpty(FamiliesFolder))
            {
                if (Directory.Exists(RootFolder))
                {
                    if (Directory.Exists(FamiliesFolder))
                        ArePathsSetted = true;
                    else
                        FamiliesFolder = String.Empty;
                }
                else
                    RootFolder = String.Empty;
            }
        }
    }
}
