﻿using EU.Plandata.Common.WPF.Models;
using EU.Plandata.Common.WPF.Services;
using EU.Plandata.Common.WPF.UserControls;
using FamilyCatalogStructureMapping.ViewModels;
using FamilyCatalogStructureMapping.Views;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace FamilyCatalogStructureMapping
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		private void Application_Startup(object sender, StartupEventArgs e)
		{
			// Initiate sample window and load theme
			var sampleWindow = new MainWindow
			{
				DataContext = new MainWindowVM()
			};

			// Load theme before showing the window (otherwise Height/Width is not set correctly)
			new ThemeService(sampleWindow).Load(Theme.CommanderDark);
			//SetLanguages();
			sampleWindow.Show();
		}

		//[DllImport("Kernel32.dll", ExactSpelling = true, CharSet = CharSet.Unicode)]
		//public static extern Boolean SetProcessPreferredUILanguages(UInt32 dwFlags,
		//	String pwszLanguagesBuffer, ref UInt32 pulNumLanguages);

		//public void SetLanguages()
		//{
		//	uint numLangs = 1;
		//	string[] langs = new string[4];
		//	uint MUI_LANGUAGE_NAME = 0x8; // Use ISO language (culture) name convention

		//	langs[0] = "es\0";
		//	langs[1] = "de-DE\0";
		//	langs[2] = "en-US\0";
		//	langs[3] = "\0"; //double null ending

		//	numLangs = (uint)langs.Length;

		//	if (SetProcessPreferredUILanguages(MUI_LANGUAGE_NAME, String.Concat(langs), ref numLangs))
		//	{
		//		if (numLangs == langs.Length - 1)
		//			Console.WriteLine("Successfully changed UI language");
		//		else if (numLangs < 1)
		//			Console.WriteLine("No language could be set");
		//		else
		//			Console.WriteLine("Not all languages were set");
		//	}
		//	else
		//		Console.WriteLine("No language could be set");
		//}
	}
}
