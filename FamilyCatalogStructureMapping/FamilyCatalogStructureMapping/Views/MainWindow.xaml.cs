﻿using EU.Plandata.Common.WPF.Services;
using FamilyCatalogStructureMapping.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FamilyCatalogStructureMapping.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public InteractionService InteractionService { get; }
        public NotificationService NotificationService { get; }
        public ThemeService ThemeService { get; }

        // Used for easy access to the current instance of the main window (used for services, etc.)
        public static MainWindow Current { get; private set; }
        public MainWindow()
        {
            var ci = new System.Globalization.CultureInfo("de-DE");
            Thread.CurrentThread.CurrentCulture = ci;

            InitializeComponent();

            // Initialize services
            InteractionService = new InteractionService(this);
            NotificationService = new NotificationService(this);

            // Set instance
            Current = this;
        }

        //private void Grid_Loaded(object sender, RoutedEventArgs e)
        //{
        //    Process.DataContext = (this.DataContext as MainWindowVM).SampleProcessVM;
        //}
    }
}
