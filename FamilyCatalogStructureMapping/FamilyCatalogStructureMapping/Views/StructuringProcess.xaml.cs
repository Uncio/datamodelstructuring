﻿using System.Threading;
using System.Windows.Controls;

namespace FamilyCatalogStructureMapping.Views
{
    /// <summary>
    /// Interaction logic for StructuringProcess.xaml
    /// </summary>
    public partial class StructuringProcess : UserControl
    {
        public StructuringProcess()
        {
            var ci = new System.Globalization.CultureInfo("de-DE");
            Thread.CurrentThread.CurrentCulture = ci;
            InitializeComponent();
        }
    }
}
