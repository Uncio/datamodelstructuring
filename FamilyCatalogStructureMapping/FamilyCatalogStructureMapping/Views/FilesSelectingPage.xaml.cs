﻿using EU.Plandata.Common.WPF.CustomControls;
using FamilyCatalogStructureMapping.ViewModels;
using System;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace FamilyCatalogStructureMapping.Views
{
    /// <summary>
    /// Interaction logic for FilesSelectingPage.xaml
    /// </summary>
    public partial class FilesSelectingPage : UserControl
    {
        GridViewColumnHeader _lastHeaderClicked = null;
        ListSortDirection _lastDirection = ListSortDirection.Ascending;
        public FilesSelectingPage()
        {
            var ci = new System.Globalization.CultureInfo("de-DE");
            Thread.CurrentThread.CurrentCulture = ci;
            InitializeComponent();
        }

        private void Page2_ClickPrevious(object sender, RoutedEventArgs e)
        {
            ((Parent as ProcessItem).Parent as Process).SelectPrevious();
        }

        private void Page2_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.Current.Close();
        }

        private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Column1.Width = 30;
            Column2.Width = 0.8 * (ListView.ActualWidth - 130) * 1/3;
            Column3.Width = 0.8 * (ListView.ActualWidth - 130) * 1/6;
            Column4.Width = 0.8 * (ListView.ActualWidth - 130) * 1 / 6;
            Column5.Width = 0.8 * (ListView.ActualWidth - 130) * 1 / 6;
            Column6.Width = 0.8 * (ListView.ActualWidth - 130) * 1 / 6;
            Column7.Width = 100;
            Column8.Width = 100;
            Column9.Width = 100;
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader headerClicked =
                e.OriginalSource as GridViewColumnHeader;
            ListSortDirection direction;

            if (headerClicked != null)
            {
                if (headerClicked.Role != GridViewColumnHeaderRole.Padding)
                {
                    if (headerClicked != _lastHeaderClicked)
                    {
                        direction = ListSortDirection.Ascending;
                    }
                    else
                    {
                        if (_lastDirection == ListSortDirection.Ascending)
                        {
                            direction = ListSortDirection.Descending;
                        }
                        else
                        {
                            direction = ListSortDirection.Ascending;
                        }
                    }

                    string header = headerClicked.Column.Header as string;
                    var sortBy = string.Empty;

                    if (String.IsNullOrEmpty(header))
                        return;
                    if (header.Equals(Properties.Resources.Column_FamilyName))
                        sortBy = "FamilyName";
                    else if (header.Equals(Properties.Resources.Column_DisciplineName))
                        sortBy = "DisciplineName";
                    else if (header.Equals(Properties.Resources.Column_CategoryName))
                        sortBy = "CategoryName";
                    else if (header.Equals(Properties.Resources.Column_ParameterTemplate))
                        sortBy = "ParameterTemplate";
                    else if (header.Equals(Properties.Resources.Column_AdditionalInfromation))
                        sortBy = "AdditionalInfo";
                    else if (header.Equals(Properties.Resources.Column_RevitCategory))
                        sortBy = "RevitCategory";

                    Sort(sortBy, direction);

                    if (direction == ListSortDirection.Ascending)
                    {
                        headerClicked.Column.HeaderTemplate =
                          Resources["HeaderTemplateArrowUp"] as DataTemplate;
                    }
                    else
                    {
                        headerClicked.Column.HeaderTemplate =
                          Resources["HeaderTemplateArrowDown"] as DataTemplate;
                    }

                    // Remove arrow from previously sorted header
                    if (_lastHeaderClicked != null && _lastHeaderClicked != headerClicked)
                    {
                        _lastHeaderClicked.Column.HeaderTemplate = null;
                    }

                    _lastHeaderClicked = headerClicked;
                    _lastDirection = direction;
                }
            }
        }

        private void Sort(string sortBy, ListSortDirection direction)
        {
            ICollectionView dataView =
                  CollectionViewSource.GetDefaultView(ListView.ItemsSource);

            dataView.SortDescriptions.Clear();
            SortDescription sd = new SortDescription(sortBy, direction);
            dataView.SortDescriptions.Add(sd);
            dataView.Refresh();
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            ListView.Items.Cast<PathStructureVM>().ToList().ForEach(x => x.IsSelected = true);
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            ListView.Items.Cast<PathStructureVM>().ToList().ForEach(x => x.IsSelected = false);
        }

        private void CheckBox_Checked_1(object sender, RoutedEventArgs e)
        {
            ListView.SelectedItems.Cast<PathStructureVM>().ToList().ForEach(x => x.IsSelected = true);
        }

        private void CheckBox_Unchecked_1(object sender, RoutedEventArgs e)
        {
            ListView.SelectedItems.Cast<PathStructureVM>().ToList().ForEach(x => x.IsSelected = false);
        }
    }
}
