﻿using EU.Plandata.Common.WPF.CustomControls;
using FamilyCatalogStructureMapping.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FamilyCatalogStructureMapping.Views
{
    /// <summary>
    /// Interaction logic for PathSettingsPage.xaml
    /// </summary>
    public partial class PathSettingsPage : UserControl
    {
        public PathSettingsPage()
        {
            var ci = new System.Globalization.CultureInfo("de-DE");
            Thread.CurrentThread.CurrentCulture = ci;
            InitializeComponent();

        }

        private void Page1_ClickNext(object sender, RoutedEventArgs e)
        {
            (DataContext as StructuringProcessVM).ReadExcelData();
            (DataContext as StructuringProcessVM).CollectFamilyFiles();
            ((Parent as ProcessItem).Parent as Process).SelectNext();
        }
    }
}
